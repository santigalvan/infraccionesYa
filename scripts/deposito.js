
function bootstrap() {

    var url = Config.url;
    var urlDeposito = '/depositos/';
    var map = createMap('mapaDeposito');



     var requestDeposito = function() {
         return $.ajax(url + urlDeposito);
     }

     var depositoIcon = L.Icon.extend({
        options: {
            iconSize:     [36, 31],
            shadowSize:   [50, 64],
            iconAnchor:   [22, 94],
            shadowAnchor: [4, 62],
            popupAnchor:  [-3, -76]
        }
      });

     //Lo ideal es separar dos metodos, uno que los dibuje en el mapa y otro que los liste
     //Falta darle un poco más de formato
     var mostrarInformacionDepositos = function(response){

          var listadoDep = document.getElementById('listadoDepositos');

          response.depositos.forEach(function(deposito){

            var elemento = document.createElement('li');
            console.log(deposito);

            var itemNombre = document.createElement('li');
            elemento.appendChild(document.createTextNode('Nombre: '+ deposito.nombre));
            elemento.appendChild(document.createElement('br'));

            var itemDireccion = document.createElement('li');
            elemento.appendChild(document.createTextNode('Dirección: '+ deposito.direccion));
            elemento.appendChild(document.createElement('br'));

            var itemTelefono = document.createElement('li');
            elemento.appendChild(document.createTextNode('Teléfono: '+ deposito.telefono));
            elemento.appendChild(document.createElement('br'));

            var itemHorario = document.createElement('li');
            elemento.appendChild(document.createTextNode('Horario de atención: '+ deposito.horarios));
            elemento.appendChild(document.createElement('br'));
            elemento.appendChild(document.createElement('br'));

            //Listamos por pantalla
            listadoDep.appendChild(elemento);
            //Dibujo en el mapa
            var icono = new depositoIcon({iconUrl: 'leaflet/images/deposito.png'});
            var p = L.marker(L.latLng(deposito.ubicacion.lat, deposito.ubicacion.lon), {icon: icono}).bindPopup(deposito.nombre);
            p.addTo(map);
          });
     }

     requestDeposito()
     .then(mostrarInformacionDepositos)
     .done(function() {
      console.log("Fin.");
      });
}
$(bootstrap);
